package linda.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

/** 
 * Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it is connected to.
 * */
public class LindaClient implements Linda {
	
    private LindaRemote lindaServer;

    /** Initializes the Linda implementation.
     *  @param serverURI the URI of the server, e.g. 
     * "rmi://localhost:4000/LindaServer" or "//localhost:4000/LindaServer".
     */
    public LindaClient(String serverURI) {
        // Get LindaServer handle
        try {
            lindaServer = (LindaRemote) Naming.lookup(serverURI);
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            throw new RuntimeException("Unable to retrieve remote LindaServer", e);
        }
    }

    @Override
    public void write(Tuple t) {
        try {
            lindaServer.write(t);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Tuple take(Tuple template) {
        try {
            return lindaServer.take(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Tuple read(Tuple template) {
        try {
            return lindaServer.read(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Tuple tryTake(Tuple template) {
        try {
            return lindaServer.tryTake(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Tuple tryRead(Tuple template) {
         try {
            return lindaServer.tryRead(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Tuple> takeAll(Tuple template) {
         try {
            return lindaServer.takeAll(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Tuple> readAll(Tuple template) {
         try {
            return lindaServer.readAll(template);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        try {
            // Send a handle to the callback, not the callback
            CallbackClient callbackClient = new CallbackClient(callback);
            lindaServer.eventRegister(mode, timing, template, callbackClient);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void debug(String prefix) {
        try {
            lindaServer.debug(prefix);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
