package linda.server;

import java.rmi.RemoteException;

import linda.Callback;
import linda.Tuple;

/**
 * The callback generated in the server side.
 */
public class CallbackServer implements Callback {

    private CallbackRemote callback;

    /**
     * Creates a CallbackServer referencing a CallbackRemote
     * @param callback the CallbackRemote to be called back
     */
    public CallbackServer(CallbackRemote callback) {
        this.callback = callback;
    }

    @Override
    public void call(Tuple t) {
        try {
            callback.call(t);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }        
    }
}
