package linda.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Launchs and register a LindaServer that can be remotly accessed.
 */
public class LaunchServer {
    
    public static void main(String[] args) {
        try {
            try { LocateRegistry.createRegistry(4000); } 
            catch (java.rmi.server.ExportException e) {
                System.out.println("Registry is already created. Proceeding...");
            }

            Registry dns = LocateRegistry.getRegistry("localhost", 4000);
            LindaServer lindaServer = new LindaServer();
            dns.bind("LindaServer", lindaServer);
            
            System.out.println("Server available in registry.");   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
