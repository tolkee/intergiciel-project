package linda.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.shm.CentralizedLinda;

/**
 * Remote access Linda implementation. This is the class on the server side.
 */
public class LindaServer extends UnicastRemoteObject implements LindaRemote {

    CentralizedLinda centralizedLinda = new CentralizedLinda();

    /**
     * Creates a new LindaServer.
     * @throws RemoteException
     */
    protected LindaServer() throws RemoteException {
        super();
    }

    @Override
    public void write(Tuple t) throws RemoteException {
        centralizedLinda.write(t);
    }

    @Override
    public Tuple take(Tuple template) throws RemoteException {
        return centralizedLinda.take(template);
    }

    @Override
    public Tuple read(Tuple template) throws RemoteException {
        return centralizedLinda.read(template);
    }

    @Override
    public Tuple tryTake(Tuple template) throws RemoteException {
        return centralizedLinda.tryTake(template);
    }

    @Override
    public Tuple tryRead(Tuple template) throws RemoteException {
        return centralizedLinda.tryRead(template);
    }

    @Override
    public Collection<Tuple> takeAll(Tuple template) throws RemoteException {
        return centralizedLinda.takeAll(template);
    }

    @Override
    public Collection<Tuple> readAll(Tuple template) throws RemoteException {
        return centralizedLinda.readAll(template);
    }

    @Override
    public void eventRegister(Linda.eventMode mode, Linda.eventTiming timing, Tuple template, CallbackRemote callback) throws RemoteException {
        // Creates a Callback that can be sent to CentralizedLinda
        CallbackServer callbackServer = new CallbackServer(callback);
        centralizedLinda.eventRegister(mode, timing, template, (Callback) callbackServer);
    }

    @Override
    public void debug(String prefix) throws RemoteException {
        centralizedLinda.debug(prefix);
    }
    
}
