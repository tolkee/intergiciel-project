package linda.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import linda.Callback;
import linda.Tuple;

/**
 * The Callback generated on the client side. It' stub can be sent to the server
 * to be called back.
 */
public class CallbackClient extends UnicastRemoteObject implements CallbackRemote {

    private Callback callback;

    /**
     * Creates a CallbackClient referencing a Callback
     * @param callback the Callback to be called back
     */
    public CallbackClient(Callback callback) throws RemoteException {
        super();
        this.callback = callback;
    }

    @Override
    public void call(Tuple t) {
        callback.call(t);
    }
    
}
