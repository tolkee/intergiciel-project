package linda.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import linda.Tuple;

/**
 * A Callback interface, but the call method can be remotly called.
 */
public interface CallbackRemote extends Remote {

    /**
     * Callback called when a tuple in the server matches t.
     * @param t the tuple
     * @throws RemoteException
     */
    public void call(Tuple t) throws RemoteException;    
}
