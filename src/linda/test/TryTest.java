package linda.test;

import linda.Linda;
import linda.Tuple;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that test tryTake and tryRead functions of CentralizedLinda
 */
public class TryTest {

    private static boolean isServerVersion = true;

    /**
     * Test that tryRead fail
     * 
     * @return if the test succeed
     */
    public static boolean shouldFailTryRead() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread t1 = new Thread(() -> {
            Tuple tuple = linda.tryRead(new Tuple(1, 2));
            result.set(tuple == null);
        });
        t1.start();
        t1.join();

        return result.get();
    }

    /**
     * Test that tryRead succeed
     * 
     * @return if the test succeed
     */
    public static boolean shouldTryRead() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread t1 = new Thread(() -> {
            linda.write(new Tuple(1, 2));
        });
        t1.start();
        t1.join();

        Thread t2 = new Thread(() -> {
            Tuple tuple = linda.tryRead(new Tuple(1, 2));
            result.set(tuple != null);
        });

        t2.start();
        t2.join();

        return result.get();
    }

    /**
     * Test that tryTake fail
     * 
     * @return if the test succeed
     */
    public static boolean shouldFailTryTake() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread t1 = new Thread(() -> {
            linda.write(new Tuple(1, 2));
        });
        t1.start();
        t1.join();

        Thread t2 = new Thread(() -> {
            Tuple tuple = linda.tryTake(new Tuple(1, 2));
            result.set(tuple != null);
        });

        t2.start();
        t2.join();

        return result.get();
    }

    /**
     * Test that tryTake succeed
     * 
     * @return if the test succeed
     */
    public static boolean shouldTryTake() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread t1 = new Thread(() -> {
            linda.write(new Tuple(1, 2));
        });
        t1.start();
        t1.join();

        Thread t2 = new Thread(() -> {
            Tuple tuple = linda.tryTake(new Tuple(1, 2));
            result.set(tuple != null);
        });

        t2.start();
        t2.join();

        return result.get();
    }

    private static Linda getALindaInstance() {
        return !isServerVersion ? new linda.shm.CentralizedLinda()
                : new linda.server.LindaClient("//localhost:4000/LindaServer");
    }

    public static void main(String[] args) {
        try {
            System.out.println("### Try methods testing ####");

            System.out.print("shouldTryRead: ");
            boolean strRes = shouldTryRead();
            System.out.println(strRes ? "✔" : "✘");

            System.out.print("shouldFailTryRead: ");
            boolean sftrRes = shouldFailTryRead();
            System.out.println(sftrRes ? "✔" : "✘");

            System.out.print("shouldTryTake: ");
            boolean sttRes = shouldTryTake();
            System.out.println(sttRes ? "✔" : "✘");

            System.out.print("shouldFailTryTake: ");
            boolean sfttRes = shouldFailTryTake();
            System.out.println(sfttRes ? "✔" : "✘");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
