package linda.test;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that test read function of CentralizedLinda
 */
public class ReadTest {

    private static boolean isServerVersion = false;

    /**
     * Test that read success
     * 
     * @return if the test succeed
     */
    public static boolean shouldRead() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread reader = new Thread(() -> {
            Tuple motif = new Tuple(Integer.class, String.class);
            Tuple res = linda.read(motif);
            result.set(res != null);
        });

        reader.start();

        Thread writer = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple("hello", 15));
            linda.write(new Tuple(4, "foo"));

        });

        writer.start();
        writer.join();
        reader.join();

        return result.get();
    }

    /**
     * Test that multiple read success
     * 
     * @return if the test succeed
     */
    public static boolean shouldRead2() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);
        ArrayList<Thread> readers = new ArrayList<>();

        for (int i = 1; i <= 3; i++) {
            final int j = i;
            Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.read(motif);
                result.set(result.get() && res != null);
            });
            readers.add(thread);
            thread.start();
        }

        Thread writer = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple("hello", 15));
            linda.write(new Tuple(4, "foo"));

        });

        writer.start();
        writer.join();
        readers.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        return result.get();
    }

    private static Linda getALindaInstance() {
        return !isServerVersion ? new linda.shm.CentralizedLinda()
                : new linda.server.LindaClient("//localhost:4000/LindaServer");
    }

    public static void main(String[] args) {
        try {
            System.out.println("### Read method testing ####");

            System.out.print("shouldRead: ");
            boolean srRes = shouldRead();
            System.out.println(srRes ? "✔" : "✘");

            System.out.print("shouldRead2: ");
            boolean sr2Res = shouldRead2();
            System.out.println(sr2Res ? "✔" : "✘");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
