package linda.test;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class that test the callbacks of CentralizedLinda
 */
public class CallBackTest {

    private static boolean isServerVersion = true;

    private static class ReadCallBack implements Callback {
        private Linda linda;
        private Tuple template;
        private AtomicInteger obs;
        private boolean oneTime;
        private boolean isTake;

        public ReadCallBack(Linda linda, Tuple template, AtomicInteger obs, boolean oneTime, boolean isTake) {
            this.linda = linda;
            this.template = template;
            this.obs = obs;
            this.oneTime = oneTime;
            this.isTake = isTake;
        }

        public void call(Tuple t) {
            obs.incrementAndGet();
            if (!oneTime)
                linda.eventRegister(isTake ? Linda.eventMode.TAKE : Linda.eventMode.READ, Linda.eventTiming.FUTURE,
                        template, this);
        }
    }

    /**
     * Test that read callback succeed
     * 
     * @return if the test succeed
     */
    public static boolean shouldReadCallback() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicInteger obs = new AtomicInteger(0);
        Tuple template = new Tuple(Integer.class, String.class);

        linda.eventRegister(Linda.eventMode.READ, Linda.eventTiming.FUTURE, template,
                new ReadCallBack(linda, template, obs, false, false));

        linda.write(new Tuple(3, "foo"));

        linda.write(new Tuple(3, 4));

        linda.write(new Tuple(2, "blah"));

        return obs.get() == 2;
    }

    /**
     * Test that read callback isn't call when a take is waiting for the same
     * templae
     * 
     * @return if the test succeed
     */
    public static boolean shouldFailReadCallback() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicInteger obs = new AtomicInteger(0);
        Tuple template = new Tuple(Integer.class, String.class);

        Thread takeThread = new Thread(() -> {
            linda.take(template);
        });

        takeThread.start();

        linda.eventRegister(Linda.eventMode.READ, Linda.eventTiming.FUTURE, template,
                new ReadCallBack(linda, template, obs, true, false));

        linda.write(new Tuple(4, "foofoo"));

        linda.write(new Tuple(3, 4));

        takeThread.join();

        return obs.get() == 0;
    }

    /**
     * Test that take callback succeed
     * 
     * @return if the test succeed
     */
    public static boolean shouldTakeCallback() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicInteger obs = new AtomicInteger(0);
        Tuple template = new Tuple(Integer.class, String.class);

        linda.eventRegister(Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, template,
                new ReadCallBack(linda, template, obs, false, true));

        linda.write(new Tuple(3, "foo"));

        linda.write(new Tuple(3, 4));

        linda.write(new Tuple(2, "blah"));

        return obs.get() == 2 && linda.tryRead(template) == null;
    }

    /**
     * Test that take callback isn't call when a take is waiting for the same
     * templae
     * 
     * @return if the test succeed
     */
    public static boolean shouldFailTakeCallback() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicInteger obs = new AtomicInteger(0);
        Tuple template = new Tuple(Integer.class, String.class);

        Thread takeThread = new Thread(() -> {
            linda.take(template);
        });

        takeThread.start();

        linda.eventRegister(Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, template,
                new ReadCallBack(linda, template, obs, true, true));

        linda.write(new Tuple(4, "foofoo"));

        linda.write(new Tuple(3, 4));

        takeThread.join();

        return obs.get() == 0;
    }

    private static Linda getALindaInstance() {
        return !isServerVersion ? new linda.shm.CentralizedLinda()
                : new linda.server.LindaClient("//localhost:4000/LindaServer");
    }

    public static void main(String[] args) {
        try {
            System.out.println("### Callback methods testing ####");
            System.out.print("shouldReadCallback: ");
            boolean srcRes = shouldReadCallback();
            System.out.println(srcRes ? "✔" : "✘");

            System.out.print("shouldFailReadCallback: ");
            boolean sfrcRes = shouldFailReadCallback();
            System.out.println(sfrcRes ? "✔" : "✘");

            System.out.print("shouldTakeCallback: ");
            boolean stcRes = shouldTakeCallback();
            System.out.println(stcRes ? "✔" : "✘");

            System.out.print("shouldFailTakeCallback: ");
            boolean sftcRes = shouldFailTakeCallback();
            System.out.println(sftcRes ? "✔" : "✘");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
