package linda.test;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that test take function of CentralizedLinda
 */
public class TakeTest {

    private static boolean isServerVersion = true;

    /**
     * Test that take success
     * 
     * @return if the test succeed
     */
    public static boolean shouldTake() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);

        Thread taker = new Thread(() -> {
            Tuple motif = new Tuple(Integer.class, String.class);
            Tuple res = linda.take(motif);
            result.set(res != null);
        });

        taker.start();

        Thread writer = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple("hello", 15));
            linda.write(new Tuple(4, "foo"));

        });

        writer.start();
        writer.join();
        taker.join();

        return result.get();
    }

    /**
     * Test that multiple take success
     * 
     * @return if the test succeed
     */
    public static boolean shouldTake2() throws InterruptedException {
        Linda linda = getALindaInstance();

        AtomicBoolean result = new AtomicBoolean(true);
        ArrayList<Thread> takers = new ArrayList<>();

        for (int i = 1; i <= 3; i++) {
            final int j = i;
            Thread taker = new Thread(() -> {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.take(motif);
                result.set(result.get() && res != null);
            });
            takers.add(taker);
            taker.start();
        }

        Thread writer = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple(4, 5));
            linda.write(new Tuple("hello", 15));
            linda.write(new Tuple(4, "foo"));

        });

        writer.start();
        writer.join();
        takers.forEach(taker -> {
            try {
                taker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        return result.get();
    }

    private static Linda getALindaInstance() {
        return !isServerVersion ? new linda.shm.CentralizedLinda()
                : new linda.server.LindaClient("//localhost:4000/LindaServer");
    }

    public static void main(String[] args) {
        try {
            System.out.println("### Take method testing ####");

            System.out.print("shouldTake: ");
            boolean stRes = shouldTake();
            System.out.println(stRes ? "✔" : "✘");

            System.out.print("shouldTake2: ");
            boolean st2Res = shouldTake2();
            System.out.println(st2Res ? "✔" : "✘");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
