package linda.shm;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

/** Shared memory implementation of Linda. */
public class CentralizedLinda implements Linda {

    List<Tuple> tuples = new ArrayList<>();

    // Tuples access locking
    ReadWriteLock lock = new ReentrantReadWriteLock();
    Lock writeLock = lock.writeLock();
    Lock readLock = lock.readLock();

    WaitingArea waitingArea = new WaitingArea(this);

    public CentralizedLinda() {
    }

    @Override
    public void write(Tuple tuple) {

        // Take the write access to tuples and write the tuple
        try {
            writeLock.lock();
            tuples.add(tuple.deepclone());
        } finally {
            writeLock.unlock();
        }

        // release awaiter waiting for a compatible template
        waitingArea.releaseAwaiters(tuple);

        // call callback saved for a compatible template
        waitingArea.callCallbacks(tuple);
    }

    @Override
    public Tuple take(Tuple template) {
        // search for a tuple matching the template (go in waiting area if not)
        Tuple tuple = waitFind(template);

        // take the write access and remove the tuple
        try {
            writeLock.lock();
            tuples.remove(tuple);
        } finally {
            writeLock.unlock();
        }

        return tuple.deepclone();
    }

    @Override
    public Tuple read(Tuple template) {
        // search for a tuple matching the template (go in waiting area if not)
        Tuple tuple = waitFind(template);
        return tuple.deepclone();
    }

    @Override
    public Tuple tryTake(Tuple template) {
        // search for a tuple matching the template
        Tuple tuple = find(template);

        // if one finded take the write access and remove it
        if (tuple != null) {
            try {
                writeLock.lock();
                tuples.remove(tuple);
            } finally {
                writeLock.unlock();
            }
        }
        return tuple == null ? null : tuple.deepclone();
    }

    @Override
    public Tuple tryRead(Tuple template) {
        // search for a tuple matching the template
        Tuple tuple = find(template);

        return tuple == null ? null : tuple.deepclone();
    }

    @Override
    public Collection<Tuple> takeAll(Tuple template) {
        Collection<Tuple> tuplesFound = new ArrayList<>();
        // take the write access, search, collect and remove all matching tuples
        try {
            writeLock.lock();
            for (Tuple tuple : tuples) {
                if (tuple.matches(template)) {
                    tuplesFound.add(tuple.deepclone());
                    tuples.remove(tuple);
                }
            }
        } finally {
            writeLock.unlock();
        }
        return tuplesFound;
    }

    @Override
    public Collection<Tuple> readAll(Tuple template) {
        // take the read access and search for all matching tuples
        try {
            readLock.lock();
            List<Tuple> matchingTuples = tuples.stream().filter(tuple -> tuple.matches(template))
                    .collect(Collectors.toList());
            return matchingTuples;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        if (timing == eventTiming.IMMEDIATE) {
            // Check if matching tuple already present
            Tuple tuple = find(template);
            if (tuple != null) {
                callback.call(tuple);
                return;
            }
        }
        // Else add callback to the waiting list
        waitingArea.addCallback(new CallbackRecord(mode, template, callback));
    }

    @Override
    public void debug(String prefix) {
        System.out.println(prefix + "debug centralized linda");
    }

    private Tuple find(Tuple template) {
        // take read acces and search for a matching tuple
        try {
            readLock.lock();

            for (Tuple tuple : tuples) {
                if (tuple.matches(template)) {
                    return tuple;
                }
            }
        } finally {
            readLock.unlock();
        }
        return null;
    }

    private Tuple waitFind(Tuple template) {
        Tuple tuple = null;
        // search a matching tuple, if not -> wait into the waiting area until a write
        // release it
        try {
            tuple = find(template);
            if (tuple == null) {
                waitingArea.addAwaiter(template).await();

                tuple = waitFind(template);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return tuple;
    }

}
