package linda.shm;

import linda.Callback;
import linda.Tuple;
import linda.Linda.eventMode;

/**
 * Stores a Callback to be called when a tuple that corresponds to the template
 * is writen in the server.
 */
public class CallbackRecord {
    
    private eventMode mode;
    private Tuple template;
    private Callback callback;

    /**
     * Creates a CallbackRecord.
     * @param mode take or read mode
     * @param template the tuple to match
     * @param callback the callback to be called back
     */
    public CallbackRecord(eventMode mode, Tuple template, Callback callback) {
        this.mode = mode;
        this.template = template;
        this.callback = callback;
    }

    /**
     * Calls the Callback stored with tuple.
     * @param tuple the tuple found
     */
    public void callback(Tuple tuple) {
        this.callback.call(tuple);
    }

    /**
     * Returns the event mode for the callback
     * @return the read or take callback mode
     */
    public eventMode getMode() {
        return this.mode;
    }

    /**
     * Returns the template expected by the Callback.
     * @return the expected template
     */
    public Tuple getTemplate() {
        return this.template;
    }

}
