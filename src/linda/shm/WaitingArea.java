package linda.shm;

import linda.Tuple;
import linda.Linda.eventMode;

import java.util.ArrayList;
import java.util.List;

/*
    A class that allows user to wait for a template off a shared memory and register callbacks
*/

public class WaitingArea {

    // awaiters on a shared Memory
    private List<Awaiter> awaiters = new ArrayList<>();

    // callbacks registered on a shared Memory
    private List<CallbackRecord> callbacks = new ArrayList<>();

    private CentralizedLinda sharedMemory;

    /**
     * Creates a Waiting Area
     * 
     * @param sharedMemory the sharedMemory
     */
    public WaitingArea(CentralizedLinda sharedMemory) {
        this.sharedMemory = sharedMemory;
    }

    /**
     * Release all the awaiter waiting for the given template
     * 
     * @param tuple the template that awaiter to release are waiting for
     */
    public synchronized void releaseAwaiters(Tuple tuple) {
        List<Awaiter> awaitersToRemove = new ArrayList<>();

        // search and elease awaiters waiting for the template
        awaiters.forEach(awaiter -> {
            if (tuple.matches(awaiter.getTemplate())) {
                awaiter.release();
                awaitersToRemove.add(awaiter);
            }
        });
        awaiters.removeAll(awaitersToRemove);
    }

    /**
     * Add an awaiter for a template
     * 
     * @param tuple the template that the awaiter is waiting for
     */
    public synchronized Awaiter addAwaiter(Tuple template) {
        Awaiter awaiter = new Awaiter(template);
        awaiters.add(awaiter);
        return awaiter;
    }

    /**
     * Call all the callbacks registered for the given template
     * 
     * @param tuple the template that the callbacks are registered for
     */
    public synchronized void callCallbacks(Tuple tuple) {
        List<CallbackRecord> callbacksToCall = new ArrayList<>();

        // search for callback watching this template and if this is as take callback,
        // take the tuple
        callbacks.forEach(callback -> {
            if (tuple.matches(callback.getTemplate())) {
                if (callback.getMode() == eventMode.TAKE) {
                    sharedMemory.take(tuple);
                }
                callbacksToCall.add(callback);
            }
        });

        // call all the finded callbacks
        for (CallbackRecord callback : callbacksToCall) {
            callback.callback(tuple);
            callbacks.remove(callback);
        }
    }

    /**
     * Add a callback
     * 
     * @param callback the Callback to be called back
     */
    public synchronized void addCallback(CallbackRecord callback) {
        callbacks.add(callback);
    }
}
