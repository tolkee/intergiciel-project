package linda.shm;

import linda.Tuple;

import java.util.concurrent.Semaphore;

// Class representing a user waiting for a tuple matching a given template

public class Awaiter {

    // Template waiting for
    private Tuple template;

    // Semaphore use to wait on
    private Semaphore lock;

    /**
     * Creates an awaiter for a template
     * 
     * @param template the template waiting for
     */
    public Awaiter(Tuple template) {
        this.template = template;
        this.lock = new Semaphore(0);
    }

    /**
     * Make the awaiter sleeping
     */
    public void await() throws InterruptedException {
        this.lock.acquire();
    }

    /**
     * Wake up the awaiter
     */
    public void release() {
        this.lock.release();
    }

    /**
     * Return the template that the awaiter is waiting for
     * 
     * @return the template
     */
    public Tuple getTemplate() {
        return template;
    }
}