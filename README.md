# Intergiciel Project

La branche `master` contient la partie SHM et l'implémentation en client / serveur.

La branche `saveToJson` contient la persistance des données.

La branche `backupServer` contient l'implémentation du serveur de secours.